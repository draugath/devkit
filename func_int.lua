test.func_int = {}
test.func_int.ui = {}

local fi = test.func_int
local ui = test.func_int.ui

fi.minor = 'funcint'
fi.functions = {}
local ftbl = fi.functions

local fc = test.fgcolor['white']
local stringify = test.stringify

local saved_functions = unspickle(gkini.ReadString(test.config.major, fi.minor..'.list', ''))
local when_list = {'IMMEDIATELY', 'PLUGINS_LOADED', 'PLAYER_ENTERED_GAME'}
local P_E_G_Timer

local function hook(fname)
	local _, f = pcall(loadstring('return '..fname))
	if (type(f) ~= 'function') then return nil end
	ftbl[fname].old = f
	local newfunc = [[	function(...)
		local fi = test.func_int
		local starttime
		local function printvals(valtype, ...)
			local endtime
			if (valtype == 'Returns') then endtime = gkmisc.GetGameTime() end
			local msg = ']]..fname..[[() '..valtype..':\n'
			for n = 1, select('#',...) do
				local e = select(n,...)
				local t = type(e)
				msg = msg..n..': ('..t..')\t'
				if (t == 'table') then
					if (e[']]..(fname:match('.*%.([%w_]+)') or fname)..[[']) then
						msg = msg.."]]..(fname:match('(.*)%.') or "** You shouldn't see this message **")..[[ ["..tostring(e)..']\n' --need double quotes on this line
					else
						msg = msg..test.stringify(e)..'\n'
					end
				else
					msg = msg..test.stringify(e)..'\n'
				end
			end
			msg = msg:gsub('\n$', '')
			if (endtime) then msg = ']]..fname..[[() Runtime: '..endtime - starttime..'ms\n'..msg end
			if (valtype == 'Arguments') then
				msg = string.rep('v', 35)..'\n'..msg
			else
				msg = msg..'\n'..string.rep('^', 30)
			end
			test.buffered_console_print(test.fgcolor['white']..msg)
			if (valtype == 'Returns') then return ... end
		end
		printvals('Arguments', ...)
		starttime = gkmisc.GetGameTime()
		return printvals('Returns', fi.functions[']]..fname..[['].old(...))
	end]]
	return pcall(loadstring(fname..'='..newfunc))
end

local function unhook(fname)
	assert(loadstring(fname..'= test.func_int.functions["'..fname..'"].old'))()
	ftbl[fname].old = nil
end

local function savelist()
	gkini.WriteString(test.config.major, fi.minor..'.list', stringify(ftbl, true))
end

function fi.add(fname, silent, init)
	local msg

	if (ftbl[fname]) then 
		msg = 'The function '..fname..'() has already been intercepted.'
		if (not silent) then OpenAlarm('Function Interceptor', msg, 'OK') end
		return
	end

	ftbl[fname] = {}
	local result, errmsg = hook(fname)
	if (result) then
		ftbl[fname].toggle = true
		ftbl[fname].when = 1
		msg = fname..'() has been successfully added to the list.'
		if (not silent) then print(fc..msg) end
		if (not init) then savelist() end
	else 
		if (result == false) then
			msg = 'The function '..fname..'() cannot be intercepted.\n'
		else
			msg = fname..' is not a function.'
		end
		if (not silent) then OpenAlarm('Function Interceptor', msg, 'OK') end
		ftbl[fname] = nil
	end
end

function fi.del(fname, silent)
	if (not ftbl[fname]) then return end

	local func = ftbl[fname]
	if (func.toggle) then fi.toggle(fname, true) end
	ftbl[fname] = nil
	savelist()
	if (not silent) then 
		local msg = fname..'() has been deleted from the list.'
		print(fc..msg) 
	end
end

function fi.delall()
	for k in pairs(ftbl) do
		fi.del(k, true)
	end
	local msg = 'All functions have been deleted from the list.'
	print(fc..msg)
end

function fi.list()
	local msg = ' function'..string.rep(' ', 41)..' | active | when\n'..
                string.rep('-', 70)..'\n'

	for k, v in pairs(ftbl) do
		msg = msg..string.format(' %-50s   %s     %s\n', k, tostring(ftbl[k].toggle), tostring(ftbl[k].when))
	end
	print(fc..'The list of hooked functions has been printed to the console.')
	console_print(msg)
end

function fi.toggle(fname, silent)
	local bool_map  = {[true] = 'ON', [false] = 'OFF'}
	if (not ftbl[fname]) then return end
	if (type(ftbl[fname].toggle) ~= 'boolean') then
		local _, t = pcall(loadstring('return type('..fname..')'))
		if (t ~= 'function') then 
			ftbl[fname].toggle = 'nil'
			print(fc..'This function does not currently exist.')
			return
		else
			ftbl[fname].toggle = false
		end
	end

	ftbl[fname].toggle = not ftbl[fname].toggle
	if (ftbl[fname].toggle) then
		hook(fname)
	else
		unhook(fname)
	end
	savelist()
	if (not silent) then
		local msg = fname..'() has been toggled '..bool_map[ftbl[fname].toggle]..'.'
		print(fc..msg)
	end
end

function fi.when(args)
	local msg
	if (args and ftbl[fname]) then
		local fname, when = args.fname, args.when
		ftbl[fname].when = when
		savelist()
		msg = fname..'() will now be intercepted '
		if (when > 1) then 
			msg = msg..'at the '..when_list[when]..' event.'
		else
			msg = msg..when_list[when]..'.'
		end
	else
		msg = '/test funcint when <function> <('
		for i, v in ipairs(when_list) do
			msg = msg..i..'='..v..'|'
		end
		msg = msg:gsub('|$', ')>')
	end
	print(fc..msg)
end

function fi.init(eventname)
	eventname = eventname or 'IMMEDIATELY'
	
	for fname, v in pairs(saved_functions) do
		if (when_list[v.when] == eventname) then
			local _, t = pcall(loadstring('return type('..fname..')'))
			if (t ~= 'function') then -- If function doesn't exist, then don't try and hook it, but keep it on the list
				v.toggle = 'nil' 
				ftbl[fname] = v
			elseif (v.toggle == 'nil') then -- If function is on the list, but didn't exist previously, leave it on the list, but don't hook it.
				v.toggle = false
				ftbl[fname] = v
			elseif (v.toggle == true) then -- Hook that bitch
				fi.add(fname, true, true)
			end
		end
	end
	if (eventname == 'PLAYER_ENTERED_GAME') then saved_functions = {} end
end

RegisterEvent(test.func_int.init, 'PLUGINS_LOADED')
RegisterEvent(function(eventname)
	P_E_G_Timer = Timer()
	P_E_G_Timer:SetTimeout(test.hook_delay, function() fi.init(eventname) P_E_G_Timer = nil end)
end, 'PLAYER_ENTERED_GAME')
fi.init()