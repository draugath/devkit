-- Originally written by slime (c) 2007
-- Heavily modified by draugath (c) 2010.

test.table_viewer     = {}
test.table_viewer.ui  = {}

local tv              = test.table_viewer
tv.minor              = 'tableviewer'
tv.failed             = false
tv.history_size       = gkini.ReadInt(test.config.major, tv.minor..'.history_size', 10)
tv.history            = unspickle(gkini.ReadString(test.config.major, tv.minor..'.history', '{}'))
tv.autoopen           = gkini.ReadInt(test.config.major, tv.minor..'.autoopen', 0) == 1
tv.treetable          = {} -- stores the tree representation of the table
tv.table_ref          = {} -- stores the references or values of items within the table displayed
tv.breadcrumbs	      = {}
tv.breadcrumbs.list   = {}

local ui              = tv.ui
ui.dialog             = {}

local xor = test.xor
local tinsert = table.insert
local tsort = table.sort

local vartypes = {
	boolean = 1,
	number = 2,
	string = 3,
	['function'] = 4,
	table = 5,
}

local function build_table_name(tbl)
	tbl = tbl or {}
	local table_ref = tv.table_ref
	local tree = ui.tree
	local curid = tonumber(tree.curid)

	local function strip_desc(id)
		local name, str = tree['NAME'..id]
-- 		if (type(table_ref[id]) == 'table') then
-- 			return (name:gsub('%(.*%)%s', ''))
-- 		else
		str = name:gsub('%s%(.-%)', '')
		str = str:gsub(':.*', '')
		if (ui.readiup.value == 'ON' and type(table_ref[id]) == 'userdata') then
			if (str == name) then return nil else return str end
		else
			return str
		end
	end
	
	while (curid > 0) do
		test.print('curid='..tostring(curid)..' / strip_desc='..tostring(strip_desc(curid))..' / type='..type(strip_desc(curid)))
		printtable{strip_desc(curid)}
		tinsert(tbl, 1, strip_desc(curid))
		if (tbl[1] == nil) then return end
		curid = tonumber(tree['PARENT'..curid])
	end
	for i, v in ipairs(test.splittable(strip_desc(0))) do
		tinsert(tbl, i, v)
	end

	return test.jointable(tbl)
end

function tv.filtertable(tbl)
	local filter = ui.filtertext.value
	test.print('Table Filter\nfiltertext='..filter)
	local function checktable(tbl, depth)
		depth = depth or 0
		local count = 0
		local newtbl = {}
		for i, v in pairs(tbl) do
			count = count + 1
			test.print('depth='..depth..' / count='..count..' / v='..tostring(v))
			if (type(v) == 'table') then
				local result = checktable(v, depth + 1)
				if (result) then 
					table.insert(newtbl, result)
				else
					count = count - 1
				end
			else
				local str = v:gsub('%s%(.-%)', '')
				str = str:gsub(':.*', '')
				if (not str:match(filter)) then
					count = count - 1
				else
					table.insert(newtbl, v)
				end
			end
		end
		if (count >= 1 and not newtbl.branchname) then 
			newtbl.branchname = tbl.branchname
		else
			if (depth == 0) then
				table.insert(newtbl, 'No table entries match the filter.')
				newtbl.branchname = tbl.branchname
				test.print('RETURN: Depth='..depth..'\n'..test.stringify(newtbl))
				return newtbl
			else
				return false
			end
		end
		test.print('RETURN: Depth='..depth..'\n'..test.stringify(newtbl))
		return newtbl
	end
	
	if (filter ~= '') then tbl = checktable(tbl) end
	return tbl
end

function tv.ConvertTable(tbl_string)
	local string_good, tbl = test.exists(tbl_string)
	local count = 0
	if (not string_good or not type(tbl)) then tv.failed = true; return false end
	tv.treetable = {}
	tv.table_ref = {}
	for k in pairs(test.dont_read) do
		local result, ref = test.exists(k)
		test.dont_read[k] = result and ref or false
	end

	local function format_val(val)
		return type(val) == 'string' and string.format('%q', val) or tostring(val)
	end
	
	local function iup_recurse(in_control, insert_table, desc)
		local controls = {}
		local cur_elem = in_control

		-- Get list of controls within the current iup container
		repeat
			tinsert(controls, cur_elem)
			cur_elem = iup.GetBrother(cur_elem)
		until (not cur_elem or desc)

		for _,v in ipairs(controls) do
			local iupType = iup.GetType(v)
			if (not desc) then 
				count = count + 1
				--test.print()
				tinsert(tv.table_ref, count, v) 
			end
			if (test.IsIupContainer(v) or (iupType == 'list' and v['control'] == 'YES')) then
				tinsert(insert_table, {branchname=(desc or iupType)})
				if (v == ui.breadcrumbbox_inner_contents) then 
					local str = 'Breadcrumb buttons are not created until later.'
					count = count + 1
					tinsert(insert_table[#insert_table], str)
					--tinsert(tv.table_ref, str)
					return 
				end
				cur_elem = iup.GetNextChild(v)
				if (cur_elem) then iup_recurse(cur_elem, insert_table[#insert_table]) end
			else
				tinsert(insert_table, (desc or iupType))
			end
		end
	end

	local function recurse(intable, insert_table)
		for _, v in pairs(test.dont_read) do 
			if (intable == v) then return end 
		end
		--if (intable == tv.treetable or intable == tv.table_ref) then return end

		local keys = {}
		if (type(intable) == 'userdata' and ui.readiup.value == 'ON' and #tv.treetable == 0) then
			iup_recurse(intable, insert_table)

		elseif (type(intable) == 'table') then
			for k in pairs(intable) do tinsert(keys, k) end
			tsort(keys, function(a,b) return gkmisc.strnatcasecmp(a,b) < 0 end)

			for _,v in ipairs(keys) do
				local t = type(intable[v])
				if (ui[t].value == 'ON') then -- verify filter setting for variable type
					count = count + 1

					if (t == 'table') then
						-- insert new table with special branchname key set, then recurse the branch
						if (type(v) == 'number') then
							tinsert(insert_table, {branchname='['..tostring(v)..'] (table)'})
						else
							tinsert(insert_table, {branchname=tostring(v)..' (table)'})
						end
						recurse(intable[v], insert_table[#insert_table])

					elseif (t == 'userdata' and 
							ui.readiup.value == 'ON' and 
							test.IsIupControl(intable[v])) then
						local desc = tostring(v)..' (iup.'..iup.GetType(intable[v])..')'
						iup_recurse(intable[v], insert_table, desc) -- don't send insert_table[#insert_table] because the decision to create a branch is first made in iup_recurse()

					else
						local val = format_val(intable[v])
						local str
						-- format key name
						if (type(v) == 'number') then str = '['..tostring(v)..']' else str = tostring(v) end
						if (t == 'string' or t == 'number') then str = str..' ('..t..'): '..val else str = str..': '..val end
						tinsert(insert_table, str)
					end
				end
			end

		else
			local val = format_val(intable)
			--if type(val) == "string" then val = string.format("%q", val) else val = tostring(val) end
			--local str = val
			--tinsert(insert_table, val)
			return true, val
		end
		return nil -- 
	end

	local result, val = recurse(tbl, tv.treetable)
	if (result) then tbl_string = tbl_string..': '..val end
	tv.treetable.branchname = tbl_string
	tv.table_ref[0] = tbl
	return true
end

function tv.breadcrumbs:New(name, path)
	local button = iup.stationbutton{title=name, path=path}

	function button:action()
		ui.textbox.value = self.path
		ui.search:action()
	end
	return button
end

function tv.breadcrumbs.create(tbl_string)
	
	local ibox = ui.breadcrumbbox_inner_contents
	if (tv.breadcrumbs.list[1] == nil) then 
		tv.breadcrumbs.list = test.splittable(ui.textbox.value) 
	end
	local bc = tv.breadcrumbs.list

	iup.Detach(ibox)
	iup.Destroy(ibox)
	ibox = iup.hbox{gap = 5,}
	
	local highindex = #bc -1
	if (highindex > 0) then
		for i = 1, highindex do
			iup.Append(ibox, tv.breadcrumbs:New(bc[i], test.jointable(bc, 1, i)))
		end
	else
		local button = tv.breadcrumbs:New(bc[1], bc[1])
		button.active = 'NO'
		iup.Append(ibox, button)
	end

	iup.Append(ui.breadcrumbbox_inner, ibox)
	ui.breadcrumbbox_inner_contents = ibox
	ui.breadcrumbbox_inner:map()
	iup.Refresh(ui.breadcrumbbox_inner)
end

function tv.create_edit_box()

	local function save_change()
-- 		local actions = {}
-- 		-- boolean
-- 		actions[1] = function(v) local bmap = {['true'] = true, ['false'] = false}; return bmap[v] end
-- 		-- number
-- 		actions[2] = function(v) return tonumber(v) end
-- 		-- strings
-- 		actions[3] = function(v) return tostring(v) end
-- 		-- function
-- 		--actions[4] = function() return pcall(loadstring(nval)) end
-- 		-- table
-- 		--actions[5] = function() return pcall(loadstring('function() return '..nval..' end')) end
-- 
-- 		local newvalue = ui.multiline.value --new value
-- 		local curtype = vartypesel.type
-- 		if (newvalue:lower() == 'nil') then 
-- 			newvalue = nil
-- 		else 
-- 			newvalue = actions[vartypes[curtype]](newvalue)
-- 			if (not newvalue) then 
-- 				OpenAlarm('Table Viewer', error, 'OK') 
-- 				return
-- 			end
-- 		end
-- 		local var = ui.selected.title
-- 		local result, error = pcall(function() local r = assert(loadstring(var..'='..cVal))() return true end)
-- 		if (result) then 
-- 			
-- 			if (curtype == 'boolean' or curtype == 'number' or curtype == 'string') then
-- 				ui.tree.
-- 				
-- 		else
-- 			OpenAlarm('Table Viewer', error, 'OK')
-- 		end
-- 			
	end

	local oType, oVal -- old type, old value
	local vartypesel = {alignment = 'ACENTER', expand = 'YES', gap = '3'}
	vartypesel[1] = iup.stationradio{title = 'boolean', active = 'NO', action = function() vartypesel.type = 'boolean' end}
	vartypesel[2] = iup.stationradio{title = 'number', active = 'NO', action = function() vartypesel.type = 'number' end}
	vartypesel[3] = iup.stationradio{title = 'string', active = 'NO', action = function() vartypesel.type = 'string' end}
	vartypesel[4] = iup.stationradio{title = 'function', active = 'NO', action = function() vartypesel.type = 'function' end}
	vartypesel[5] = iup.stationradio{title = 'table', active = 'NO', action = function() vartypesel.type = 'table' end}
	local function set_radio_value(self, v)
		test.print('self='..tostring(self)..' / v='..tostring(v))
		local t = type(v)
		test.print('t='..t)
		if (t ~= 'number' and t ~= 'string') then 
			self.value.value = 'OFF'
			self.value = nil
			return nil 
		end
		if (t == 'string') then
			v = vartypes[v]
		else
			if (v >5 or v<1) then test.print('not in range') return nil end
		end
		test.print('v='..tostring(v))
		self.value = vartypesel[v]
		if (v) then vartypesel[v].action() end
		return true
	end
	ui.radio = iup.radio{iup.hbox(vartypesel), active = 'YES', setvalue = set_radio_value}
	local save = iup.stationbutton{title = 'Save', active = 'NO', action = save_change}
	ui.multiline = iup.stationsubmultiline{size = '350', expand = 'yes', readonly = 'yes'}
	local function edit_value(self)
		if (self.title == 'Edit') then
			oVal = ui.multiline.value
			oType = vartypesel.type
			ui.multiline.readonly = 'NO'
			for i, v in ipairs(vartypesel) do if (i > 3) then break end v.active = 'YES' end
			save.active = 'YES'
			self.title = 'Cancel'
		else
			ui.multiline.value = oVal
			if (vartypesel.type ~= oType) then ui.radio:setvalue(oType) end
			ui.multiline.readonly = 'YES'
			for i, v in ipairs(vartypesel) do if (i > 3) then break end v.active = 'NO' end
			save.active = 'NO'
			self.title = 'Edit'
		end
	end
	local edit = iup.stationbutton{title = 'Edit', action = edit_value}
	local edit_box = iup.vbox{
		iup.hbox{
			ui.radio,
			iup.fill{},
			save,
			edit,
			gap = 3,
		},
		ui.multiline,
		gap = 5,
	}
	return edit_box
end


-- Top line controls --------
ui.entry, ui.textbox, ui.dropdown = test.list2{expand='HORIZONTAL', list = tv.history}
ui.dropdown.oldaction = ui.dropdown.action
ui.dropdown.action = function(self, t, i, v) self.oldaction(self, t, i, v); if tv.autoopen then ui.search.action() end end

ui.readiup = iup.stationtoggle{title = 'Read IUP', value = 'OFF', action = function(self)
		ui.search.action()
	end
}
ui.search = iup.stationbutton{title='Go', size=100, action=function(self, fromOpen)
		local tbl = ui.textbox.value
		if tbl == '' then return end
		if (fromOpen == nil) then tv.breadcrumbs.list = {} end
		if (test.disable_iup_read[tbl]) then ui.readiup.value = 'OFF' end

		if (tv.ConvertTable(tbl)) then
			tv.treetable = tv.filtertable(tv.treetable)
			-- Recreate the tree
			ui.tree.value = 'ROOT'
			ui.tree.delnode = 'CHILDREN'
			iup.TreeSetValue(ui.tree, tv.treetable)
			ui.tree.redraw = 'YES'

			-- Set/create GUI elements
			ui.tree:selection_cb(0)
			tv.breadcrumbs.create(tbl)

			tv.failed = false

			-- Check the history and add new search item if necessary
			local inHistory = false
			for _, v in ipairs(tv.history) do if (v == tbl) then inHistory = true; break end end
			if (not inHistory) then 
				tinsert(tv.history, 1, tbl)
				local size = tv.history_size
				if (#tv.history == (size + 1)) then tv.history[size + 1] = nil end
				do
					local h = {}
					for i = 1, size do h[i] = tv.history[i] end
					gkini.WriteString(test.config.major, tv.minor..'.history', spickle(h))
				end
			end
			for i, v in ipairs(tv.history) do ui.dropdown[i] = v end
		end
	end
}
-- --------------------------
-- Filter Box definition ----
ui.filterbox = iup.hbox{gap = 10, iup.label{title = 'Filters:'}}
for _, v in ipairs({'boolean', 'number', 'string', 'function', 'userdata', 'thread', 'table'}) do
	ui[v] = iup.stationtoggle{title = v, value = 'on', action = function(self)
		ui.search.action()
	end}
	iup.Append(ui.filterbox, ui[v])
end
-- --------------------------
-- Breadcrumb Box definition
ui.breadcrumbbox_inner_contents = iup.hbox{gap = 5,}
ui.breadcrumbbox_inner = iup.hbox{ui.breadcrumbbox_inner_contents}
-- --------------------------
-- Other controls
ui.selected = iup.label{title = '', expand = 'HORIZONTAL'}
ui.open = iup.stationbutton{title = 'Open', active = 'NO', action = function(self)
		tv.breadcrumbs.list = {}
		local bc = tv.breadcrumbs.list
		local str = build_table_name()

		if (test.dont_read[str]) then return end
		ui.textbox.value = str
		ui.search:action(true)
	end
}

ui.tree = iup.stationsubsubtree{expand='YES', addexpanded='NO', renamenode_cb=function(self, id, name)
		if self['KIND'..id] == 'BRANCH' then
			local state = self['STATE'..id]
			if state == 'EXPANDED' then
				self:setstate(id, 'COLLAPSED')
			else
				self:setstate(id, 'EXPANDED')
			end
		end
	end, 
	selection_cb = function(self, id, status)
		if (ui.textbox.value ~= nil and ui.textbox.value == '') then return iup.IGNORE end
		local name, ref, val

		if (id == 0) then ui.open.active = 'NO' else ui.open.active = 'YES' end
		self.curid = id -- type(id) == 'number', but it is getting converted to 'string' on assignment
		name = build_table_name()
		test.print('id = '..id..' / name = '..tostring(name))
		-- If no table_ref entry, get the ref or value form the name.  test.exists() returns the value in it's second return.
		ref = tv.table_ref[id] or assert(function() local _, v = test.exists(name); return v end)()
		if (ref == 'attempt to call a nil value') then ref = nil end
		test.print('ref = '..tostring(ref))

		if (ui.readiup.value == 'ON' and test.IsIupControl(ref)) then
			val = 'Name='..test.nz(iup.GetName(ref)).."\n"..string.gsub(iup.GetAttributes(ref),",", "\n")
		else
			val = tostring(ref)
		end
		test.print('val = '..tostring(val))
		ui.multiline.value = val
		ui.radio:setvalue(type(ref))
		ui.selected.title = name
	end
}

ui.filtertext = iup.text{expand = 'HORIZONTAL'}
ui.filterbutton = iup.stationbutton{title = 'Filter', size = 100, action = ui.search.action}
ui.options = iup.stationbutton{title='Options', size = 100, action = function(self)
		local opt = tv.options
		opt.build_dialog()
		opt.ui.dialog:popup(iup.CENTER, iup.CENTER)
		opt.ui.dialog:destroy()
	end}


ui.close = iup.stationbutton{title='Close', size = 100, action = function(self)
		HideDialog(ui.dialog)
	end
}
-- -----------------------------

function tv.build_dialog()
	ui.main = iup.pdarootframe{
		iup.pdarootframebg{
			iup.hbox{
				iup.vbox{
					iup.hbox{
						iup.label{title='Enter A Table:'},
						ui.entry,
						ui.readiup,
						iup.fill{},
						ui.search,
						gap=5, alignment='ACENTER',
					},
					ui.filterbox,
					iup.hbox{ui.breadcrumbbox_inner,},
					iup.hbox{
						iup.label{title = 'Currently selected: '},
						ui.selected, 
						ui.open, 
						alignment = 'ACENTER'
					},
					iup.hbox{
						ui.tree,
						tv.create_edit_box(),
						gap=5,
					},
					iup.hbox{ui.filtertext, ui.filterbutton, iup.fill{expand = 'YES'}, ui.options, ui.close, gap = 5},
					gap=5,
				},
			},
		},
		size="TWOTHIRDxTWOTHIRD", expand="NO",
	}

	ui.dialog = iup.dialog{
		iup.vbox{
			iup.fill{},
			iup.hbox{
				iup.fill{},
				ui.main,
				iup.fill{},
			},
			iup.fill{},
		},
		defaultesc=ui.close,
		defaultenter=ui.search,
		bgcolor="0 0 0 128 *",
		-- fullscreen="YES",
		title = 'Table Viewer [v'..test.version..']',
		border="NO",
		resize="NO",
		menubox="YES",
		topmost="YES",
	}

	function ui.dialog:Show(str)
		local autoset = false
		if (str ~= nil and type(str) == 'string') then ui.textbox.value = str; autoset = true end
		ShowDialog(self, iup.CENTER, iup.CENTER)
		if (autoset) then ui.search:action() end
	end
	ui.dialog:map()
end

dofile('table_viewer_options.lua')