test.event_mgmt                 = {}
test.event_mgmt.toggle          = {}
test.event_mgmt.build_dialog    = {}
test.event_mgmt.ui              = {}
test.event_mgmt.ui.dialog       = {}

local em = test.event_mgmt
local ui = em.ui

em.minor = 'eventmanager'

--                                test.events[num index] 
--                                              ^   ^
--                                              |   : 
--  test.events_ref.reverse_map[event_name key]-/   ...test.events_ref.registered[num index]
--                                     |
--                                     v
--     test.events_ref.iup_controls[num index]

-- test.events_ref.reverse_map holds a numeric index corresponding to the event_name, used as a key, for accessing
--     the event within test.events and the iup.toggle within test.events_ref.iup_controls

-- test.events_ref.registered is accessed via the numeric index of the event and holds a reference to the data
--     in test.events

local state_map = {ON = true, OFF = false}
local bool_map  = {[true] = 'ON', [false] = 'OFF'}

function test.event_mgmt.init()
	em.load_defaults(true)
	em.build_dialog()
	em:apply_search()
end

function test.event_mgmt.load_defaults(init)
	local notes = unspickle(gkini.ReadString(test.config.major, em.minor..'.defaults', ''))
--	if (#notes ~= 0) then test.events = notes else test.events = test.events_default end
	-- Make sure that no events are currently registered
	if (assert(function() for k in pairs(test.events_ref.registered) do return true end return false end)()) then
		em.unregister_all()
	end

	-- Register defaults
		for _, v in ipairs(notes) do
			em.manual_toggle('reg', test.events[v][1], true, true)
		end

		--if (state_map[v[2]]) then em.register_events(v) end

end

function test.event_mgmt.readevent(event, ...)
	if (not test.debug.events) then return end
	local str = ''
	local details = test.events[test.events_ref.reverse_map[event]][3]

	for n = 1, select('#',...) do
		local e = select(n,...)
		local t = type(e)
		str = str..'\n\t'..n..': ('..t..')\t'..test.stringify(e)

		if (test.debug.details and type(details[n]) == 'function') then 
			local success, d = pcall(details[n], e)
			if (success and d) then str = str..' ('..d..')' end
		end
	end

	console_print("Event: "..event.." - Data:"..str)
end

function test.event_mgmt.register_events(tbl)
	-- Accepts a table and (un)registers an event based on the value of the second index
	local event, state = unpack(tbl)
	local idx = test.events_ref.reverse_map[event]

	if (state_map[state]) then
		RegisterEvent(test.event_mgmt.readevent, event)
		test.events_ref.registered[idx] = test.events[idx]
	else
		UnregisterEvent(test.event_mgmt.readevent, event)
		test.events_ref.registered[idx] = nil
	end
end

function test.event_mgmt.manual_toggle(action, event, silent, init)
-- manual_toggle(action, event [,silent [, init]])
	--event = event:upper()
	--if (event == 'RHUDXSCALE') then event = 'rHUDxscale' end
	local idx = test.events_ref.reverse_map[event]

	if (idx == nil) then print('Unknown event name. ('..event..')'); return end

	if (action == 'reg') then
		if (not state_map[test.events[idx][2]] or init) then 
			test.events[idx][2] = 'ON'
			pcall(function() test.events_ref.iup_controls[idx].value = test.events[idx][2] end)
			em.register_events(test.events[idx])
		else
			action = 'is already '..action
		end
	elseif (action == 'unreg') then
		if (state_map[test.events[idx][2]]) then 
			test.events[idx][2] = 'OFF'
			test.events_ref.iup_controls[idx].value = test.events[idx][2]
			em.register_events(test.events[idx])
		else
			action = 'is already '..action
		end
	end
	if (not silent) then print('Event '..action..'istered: '..event) end
end

function test.event_mgmt.event_status(getlistonly)
--event_status([getlistonly])
	local count = 0
	local event_list = {}

	for _,v in pairs(test.events_ref.registered) do
		table.insert(event_list, v[1])
		count = count + 1
	end

	if (not getlistonly) then
		print('The list of registered events has been printed to the console.')
		console_print('Registered Events\n'..
					'---------------------------')
		if (count == 0) then 
			console_print('\tnone')
		else
			console_print('\t'..table.concat(event_list, '\n\t')) 
		end
	end
	return event_list
end

function test.event_mgmt.unregister_all()
	local reg_events = em.event_status(true)
	for _, v in ipairs(reg_events) do
		em.manual_toggle('unreg', v, true)
	end
end

local function subdlg(ctrl)
	local dlg = iup.dialog{
		ctrl,
		border="NO",
		menubox="NO",
		resize="NO",
		bgcolor="0 0 0 0 *",
	}
	return dlg
end

local function detach_events()
	local detach_children
	detach_children = function(element, levels)
		local levels = tonumber(levels) or 0

		if levels >= 1 then
			local child = iup.GetNextChild(element)
			while child do
				detach_children(child, levels-1)
				iup.Detach(child)
				child = iup.GetNextChild(element)
			end
		end
	end

	local subdlg = iup.GetNextChild(ui.listbox)
	if not subdlg then return end --already detached
	local list = iup.GetNextChild(subdlg)

	ui.listbox[1] = nil
	detach_children(subdlg, 3)
	iup.Destroy(subdlg)
	iup.Destroy(list)
end
-- ----------------------------------------------------------------------------

function test.event_mgmt.toggle:New(tbl)
	local event, state = unpack(tbl)
	local tog = iup.stationtoggle{title = event, value = state}

	function tog:action()
		local idx = test.events_ref.reverse_map[self.title]
		test.events[idx][2] = self.value
		em.register_events(test.events[idx])
	end
	return tog
end

ui.debugevents = iup.stationtoggle{title = 'Debug Events', action = function(self, v)
		if (v == nil) then 
			self.value = bool_map[test.debug.events]
		else
			test.debug.events = state_map[self.value]
		end
		gkini.WriteInt(test.config.major, 'debug.events', test.debug.events and 1 or 0)
	end
}

ui.details = iup.stationtoggle{title = 'Details', action = function(self, v)
		if (v == nil) then 
			self.value = bool_map[test.debug.details]
		else
			test.debug.details = state_map[self.value]
		end
		gkini.WriteInt(test.config.major, 'debug.details', test.debug.details and 1 or 0)
	end
}

ui.selectall = iup.stationbutton{title = 'Enable Visible', action = function() --set title to 'Enable Visible' because longer than 'Enable All' to size button correctly
		for event,_ in pairs(test.events_ref.displayed) do
			em.manual_toggle('reg', event, true)
		end
	end
}

ui.clearall = iup.stationbutton{title = 'Disable Visible', action = function()
		for event,_ in pairs(test.events_ref.displayed) do
			em.manual_toggle('unreg', event, true)
		end
	end
}

ui.load = iup.stationbutton{title = 'Load Defaults', action = function()
		em.unregister_all()
		em.load_defaults()
	end
}

ui.save = iup.stationbutton{title = 'Save Defaults', action = function()
		local notes = {}
		for _, v in ipairs(em.event_status(true)) do
			table.insert(notes, test.events_ref.reverse_map[v])
		end
		gkini.WriteString(test.config.major, em.minor..'.defaults', spickle(notes))
	end
}

ui.searchtext = iup.text{value="", expand="HORIZONTAL", action=function(self,key)
	if key==iup.K_CR or key==13 then
		em:apply_search()
	end
end}

ui.searchbutton = iup.stationbutton{title = 'Go', action = function()
		em:apply_search()
end}

ui.clearsearch = iup.stationbutton{title = 'Clear', action = function()
		ui.searchtext.value = "" --clear search string
		em:apply_search()
		iup.SetFocus(ui.searchtext)
	end
}

ui.close = iup.stationbutton{title="Close", expand="HORIZONTAL", action = function(self)
		HideDialog(ui.dialog)
	end
}

ui.listbox = iup.stationsublist{{}, control = "YES", expand = "YES",}

function test.event_mgmt:apply_search(search_str)
	search_str = search_str or ui.searchtext.value
	ui.searchtext.value = search_str

	local last_char, cur_char
	local list = iup.vbox{}
	local vbox

	--remove all events
	detach_events()
	test.events_ref.displayed = {}
	local all_events_shown = true --assume true until proven false
	local no_events_shown = true

	--add event matching search querry
	for i,v in ipairs(test.events) do
		if string.match(v[1], search_str) then			
			-- Get and check first letter for section label
			cur_char = string.match(v[1], "^[%a%u]")
			if (cur_char ~= last_char) then 
				if (last_char) then -- Add end-of-section spacing
					iup.Append(list, vbox)
					iup.Append(list, test.events_ref.iup_letter_groups[last_char][3])
				end
				--Add new section label
				last_char = cur_char
				vbox = test.events_ref.iup_letter_groups[cur_char][1]
				iup.Append(vbox, test.events_ref.iup_letter_groups[cur_char][2])
			end
			iup.Append(vbox, test.events_ref.iup_hboxes[v[1]])

			test.events_ref.displayed[v[1]] = true
			no_events_shown = false
		else
			all_events_shown = false
		end
	end
	if vbox then iup.Append(list, vbox) end

	if no_events_shown then --search resulted in no matches
		vbox = test.events_ref.iup_letter_groups.NO_EVENTS[1]
		iup.Append(vbox, test.events_ref.iup_letter_groups.NO_EVENTS[2])
		iup.Append(list, vbox)

		ui.selectall.active = "OFF"
		ui.clearall.active = "OFF"
	else
		ui.selectall.active = "ON"
		ui.clearall.active = "ON"
	end

	iup.Append(ui.listbox, subdlg(list))
	ui.listbox[1] = 1

	--retain max size of select/clear buttons
	local selectall_size = ui.selectall.size
	local clearall_size = ui.clearall.size

	ui.selectall.title = all_events_shown and "Enable All" or "Enable Visible"
	ui.clearall.title = all_events_shown and "Disable All" or "Disable Visible"
	ui.clearsearch.active = all_events_shown and "OFF" or "ON"

	iup.Map(ui.dialog)

	ui.selectall.size = selectall_size
	ui.clearall.size = clearall_size

	iup.Refresh(ui.dialog)
end

function test.event_mgmt.build_dialog()
	local listbox = ui.listbox
	local last_char, cur_char
	local hbox = {}
	local vbox

	-- Create iup.toggle elements for each event
	for i, v in ipairs(test.events) do
		-- Get and check first letter for section label
		cur_char = string.match(v[1], "^[%a%u]")
		if (cur_char ~= last_char) then 
			if (last_char) then -- Add end-of-section spacing
				test.events_ref.iup_letter_groups[last_char][3] = iup.fill{size = 25}
			end
			--Add new section label
			last_char = cur_char
			vbox = iup.vbox{gap = 2}
			test.events_ref.iup_letter_groups[cur_char] = {vbox}
			test.events_ref.iup_letter_groups[cur_char][2] = iup.hbox{iup.label{title = cur_char}, iup.fill{}}
		end
		test.events_ref.iup_controls[i] = em.toggle:New(v)
		test.events_ref.iup_hboxes[v[1]] = iup.hbox{test.events_ref.iup_controls[i], iup.fill{}}
	end

	--create message for case where no events matching search found
	test.events_ref.iup_letter_groups.NO_EVENTS = {iup.vbox{gap = 2}}
	test.events_ref.iup_letter_groups.NO_EVENTS[2] = iup.hbox{iup.fill{}, iup.label{title = "-- NO MATCHES FOUND --"}, iup.fill{}}

	ui.debugevents.value = bool_map[test.debug.events]
	ui.details.value = bool_map[test.debug.details]

	ui.main = iup.pdarootframe{
		iup.pdarootframebg{
			iup.vbox{
				iup.hbox{
					iup.label{title="Events"},
					iup.fill{expand = 'YES'},
					ui.debugevents,
					ui.details,
					iup.fill{size = 20},
					ui.selectall,
					ui.clearall,
					iup.fill{size = 20},
					ui.load,
					ui.save,
					gap = 10,
				},
				iup.hbox{
					iup.label{title="Search:"},
					iup.fill{size = 10},
					ui.searchtext,
					ui.searchbutton,
					iup.fill{size = 10},
					ui.clearsearch,
					iup.fill{expand = 'YES'},
				},
				listbox,
				ui.close,
				gap=5,
			},
		},
		size="TWOTHIRDxTWOTHIRD", expand="NO",
	}

	ui.dialog = iup.dialog{
		iup.vbox{
			iup.fill{},
			iup.hbox{
				iup.fill{},
				ui.main,
                iup.fill{},
			},
			iup.fill{},
		},
		defaultesc=ui.close,
		bgcolor="0 0 0 128 *",
		fullscreen="YES",
		border="NO",
		resize="NO",
		menubox="NO",
		topmost="YES",
	}
	ui.dialog:map()
end

function test.event_mgmt.ui.Open()
	ShowDialog(ui.dialog)
end
-- function test.event_mgmt.ui.Close()
-- 	HideDialog(test.event_mgmt.ui.dialog)
-- end
-- test.event_mgmt.ui.close.action = test.event_mgmt.ui.Close

test.event_mgmt.init()
