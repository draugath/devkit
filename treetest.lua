declare('treetest', {})
local stringify = test.stringify
local readiup = {value='ON'}
--[[
 When setting the value, all acceptable action values appear to only select root.
]]


local tinsert = table.insert
local tsort   = table.sort

function treetest.PopulateTree(tree, table_name)
	local tbl_exists, tbl = test.exists(table_name)
	if (not tbl_exists) then return false end

	-- Create references for dont_read
	for k in pairs(test.dont_read) do
		local result, ref = test.exists(k)
		test.dont_read[k] = result and ref or false
	end

	local function format_val(val)
		return type(val) == 'string' and string.format('%q', val) or tostring(val)
	end
	
	local function iup_recurse(control, node, label)
		local cur_control = control
		test.print('iup_recurse -- control='..iup.GetType(control)..' / node='..tostring(node)..' / label='..tostring(label))

		-- Get list of controls within the current iup container
		local controls = {}
		repeat
			tinsert(controls, cur_control)
			cur_control = iup.GetBrother(cur_control)
		until (not cur_control or label)
		test.print('controls='..test.stringify(controls))
		
		for i=#controls, 1, -1 do -- process controls table in reverse for adding to tree
			local v = controls[i]
			local value = v
			local iupType = iup.GetType(v)

			if (test.IsIupContainer(v) or (iupType == 'list' and v['control'] == 'YES')) then

-- 				if (v == breadcrumbs) then 
-- 					tree:leafadd(node, 'Breadcrumb buttons are not created until later.')
-- 					return 
-- 				end

				if (node == -1) then
					tree:associate(0, value)
				else
					if (label) then value = nil end
					tree:branchadd(node, label or iupType, value)
				end
				cur_control = iup.GetNextChild(v)
				if (cur_control) then iup_recurse(cur_control, node + 1) end
			else
				if (node == -1) then
					tree:associate(0, value)
				else
					if (label) then value = nil end
					tree:leafadd(node, label or iupType, value)
				end
			end
		end
	end

	local function recurse(tbl, node)
		node = node or 0
		test.print('recurse start -- node='..node)
		
		for _, v in pairs(test.dont_read) do 
			if (tbl == v) then return end 
		end

		if (type(tbl) == 'userdata' and readiup.value == 'ON' and tree['name1'] == nil) then
			iup_recurse(tbl, -1) -- Send node -1 to indicate root level should be changed

		elseif (type(tbl) == 'table') then
			local keys = {}
			for k in pairs(tbl) do tinsert(keys, k) end
			tsort(keys, function(a,b) return gkmisc.strnatcasecmp(a,b) > 0 end)
			test.print('keys='..test.stringify(keys))
			test.print('tbl='..test.stringify(tbl))
			
			for _,v in ipairs(keys) do
				local t = type(tbl[v])
				--if (filters[t].value == 'ON') then -- verify filter setting for variable type
				test.print('v='..tostring(v)..' / t='..t)
				
					local label = tostring(v)
					if (t == 'table') then
						-- insert new table with special branchname key set, then recurse the branch
						
						if (type(v) == 'number') then
							label = '['..label..'] (table)'
						else
							label = label..' (table)'
						end
						tree:branchadd(node, label)
						recurse(tbl[v], node + 1)

					elseif (t == 'userdata' and readiup.value == 'ON' and test.IsIupControl(tbl[v])) then
						label = label..' (iup.'..iup.GetType(tbl[v])..')'
						iup_recurse(tbl[v], node, label) -- don't send node == node + 1 because the decision to create a branch is first made in iup_recurse()

					else
						local val = format_val(tbl[v])
						-- format key name
						if (type(v) == 'number') then label = '['..label..']' end
						if (t == 'string' or t == 'number') then label = label..' ('..t..'): '..val else label = label..': '..val end
						tree:leafadd(node, label)
					end
				--end
			end

		else
			local val = format_val(tbl)
			return true, val
		end
		return nil -- 
	end

	local result, val = recurse(tbl)
	if (result) then table_name = table_name..': '..val end
	tree['name0'] = table_name
	return true
end



treetest.tree = iup.tree{size="TWOTHIRDxTWOTHIRD"}
local tree = treetest.tree

tree.userdata_ref = {}

function tree:associate(id, value)
	tinsert(self.userdata_ref, value)
	iup.TreeSetTableId(self, id, self.userdata_ref[#self.userdata_ref])
end

function tree:clearnode(id)
	id = 0 -- TODO: add support for clearing child nodes someday, for now only entire tree
	for _, v in ipairs(tree.userdata_ref) do
		iup.TreeSetTableId(self, iup.TreeGetTableId(self,v), nil)
	end
	tree.userdata_ref = {}
	tree['delnode0'] = 'CHILDREN'
end

function tree:branchadd(id, label, value)
	self['addbranch'..id] = label
	self['state'..id + 1] = 'COLLAPSED'
	if (value) then self:associate(id + 1, value) end
end

function tree:leafadd(id, label, value)
	self['addleaf'..id] = label
	if (value) then self:associate(id + 1, value) end
end

function tree:selection_cb(id, status)
	local t = iup.TreeGetTable(self, id)
	local str = 'SELECTION_CB: id='..stringify(id)..' / status='..stringify(status)
	--if (status == 1 and t) then str = str..' || t[1]='..stringify(t[1])..' / t[2]='..stringify(t[2]) end
	test.print(str)
	return iup.IGNORE
end

function tree:multiselection_cb(ids, n)
	test.print('MULTISELECTION_CB: ids='..stringify(ids)..' / status='..stringify(status))
	return iup.IGNORE
end

function tree:branchopen_cb(id)
	test.print('BRANCHOPEN_CB: id='..stringify(id))
	return iup.DEFAULT
end
	
function tree:branchclose_cb(id)
	test.print('BRANCHCLOSE_CB: id='..stringify(id))
	return iup.DEFAULT
end

function tree:executeleaf_cb(id)
	test.print('EXECUTELEAF_CB: id='..stringify(id))
	return iup.DEFAULT
end

function tree:renamenode_cb(id, name)
	test.print('RENAMENODE_CB: id='..stringify(id)..' / name='..stringify(name))
	return iup.DEFAULT
end

function tree:showrename_cb(id)
	test.print('SHOWRENAME_CB: id='..stringify(id))
	return iup.DEFAULT
end

function tree:rename_cb(id, name)
	test.print('RENAME_CB: id='..stringify(id)..' / name='..stringify(name))
	return iup.DEFAULT
end

function tree:rightclick_cb(id)
	test.print('RENAMENODE_CB: id='..stringify(id)..' / name='..stringify(name))
	return iup.DEFAULT
end

function treetest.addbranch(tree, label)
	local id = tree.value
	tree['addbranch'..id] = label
	iup.TreeSetTableId(tree, id+1, {id+1, label})
end

function treetest.addleaf(tree, label)
	local id = tree.value
	tree['addbranch'..id] = label
	iup.TreeSetTableId(tree, id+1, {id+1, label})
end

tree.name0 = "root"


ShowDialog(iup.dialog{
	topmost = 'YES', 
	iup.vbox{
		tree, 
	}
})

function treetest.test()
	treetest.tbl = {
		branch1={
			subbranch1={
				'blah',
				'maybe', 
				'whee'
			}, 
			'something'
		}, 
		branch2={
			test2='hmmm', 
			test='not quite', 
			{
				version='asdf', 
				blech={}
			}
		}, 
		leaf1='leaf1', 
		aardvark='leaf2'
	}
	treetest.PopulateTree(tree, 'treetest.tbl')
end