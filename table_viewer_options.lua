local tv = test.table_viewer
local ui = tv.ui

tv.options = {}
tv.options.ui = {}

local opt = tv.options
local optui = opt.ui
local size = tv.history_size

local state_map = {ON = true, OFF = false}
local bool_map  = {[true] = 'ON', [false] = 'OFF'}
local val_map   = {[true] = 1, [false] = 0}

function tv.options.build_dialog()
	optui.autoopen = iup.stationtoggle{title = 'Auto-open from history', value = bool_map[tv.autoopen]}
	optui.size = iup.text{value = size, size = 25}
	optui.reset = iup.stationbutton{title = 'Reset History', size = 100, action = function(self)
			for i = 1, size do ui.dropdown[i] = nil end
			tv.history = {}
			gkini.WriteString(test.string.major, tv.minor..'.history', spickle(tv.history))
		end}
	optui.save = iup.stationbutton{title = 'Save', size = 75, action = function(self)
			local oldsize = size
			size = tonumber(optui.size.value)
			if size < oldsize then
				for i = oldsize, size + 1, -1 do
					ui.dropdown[i] = nil
					tv.history[i] = nil
				end
			end
			gkini.WriteInt(test.config.major, tv.minor..'.history_size', size)
			tv.history_size = size
			tv.autoopen = state_map[optui.autoopen.value]
			gkini.WriteInt(test.config.major, tv.minor..'.autoopen', val_map[tv.autoopen])
			optui.cancel:action()
		end}

	optui.cancel = iup.stationbutton{title = 'Cancel', size = 75, action = function(self) 
			iup.GetDialog(self):hide()
		end}

	optui.main = iup.pdarootframe{
		iup.pdarootframebg{
			iup.vbox{
				iup.fill{},
				iup.hbox{
					iup.fill{},
					iup.vbox{
						iup.hbox{optui.autoopen},
						iup.hbox{iup.label{title = 'History Size: '}, optui.size},
						iup.hbox{optui.reset},
						gap = 5,
					},
					iup.fill{},
				},
				iup.hbox{
					iup.fill{},
					optui.save,
					optui.cancel,
					iup.fill{},
					gap = 10,
				},
				gap=10,
				iup.fill{},
			},
		},
		expand="NO",
	}

	optui.dialog = iup.dialog{
		iup.vbox{
			iup.fill{},
				iup.hbox{
					iup.fill{},
					optui.main,
					iup.fill{},
				},
			iup.fill{},
		},
		defaultesc = optui.cancel,
		--bgcolor = "0 0 0 128 *",
		border = "NO",
		resize = "NO",
		menubox = "NO",
		topmost = "YES",

	}

	optui.dialog:map()
end