test.config             = {
	major       = 'DevKit',
}

test.debug              = {
	details      = (gkini.ReadInt(test.config.major, 'debug.details', 0) == 1), --activates extra details on return values from events
	events       = (gkini.ReadInt(test.config.major, 'debug.events', 0) == 1),
	print        = (gkini.ReadInt(test.config.major, 'debug.print', 1) == 1),
}

test.notesid            = 8378

test.events_ref = {
	iup_controls = {}, --iup toggle controls only
	iup_hboxes = {}, --iup hbox containing iup toggle control plus iup fills
	iup_letter_groups = {}, --letter as key, sub-table contains: [1]=vbox, [2]=hbox with letter header, [3]=end-of-section fill
	registered = {},
	reverse_map = {},
	displayed = {}, --sublisting of events matching search string. Event name as key, true if event visible
}

test.container_types = {
	canvas = true,
	dialog = true,
	frame = true, 
	hbox = true,
	submenu = true,
	menu = true,
	radio = true,
	vbox = true,
	zbox = true,
	cbox = true,
}

test.disable_iup_read = {
-- 	['HUD'] = true,
-- 	['tcs'] = true,
}

test.dont_read = {
-- 	['test.table_viewer.table_ref'] = true,
-- 	['test.table_viewer.treetable'] = true,
}

test.fgcolor = {
	white = '\127ffffff',
}

test.buffered_console_print_delay = 0

test.hook_delay = 10000

test.help = {
	main = [[Test help: /test [command] help
Commands:
	debug				-- show status of debug message printing
	events				-- open the Event Manager
	funcint				-- function interceptor
	table					-- open the Table Viewer]],

	debug = [[Test Debug help: /test debug <sub-command>
Sub-commands:
	details				-- toggle printing of extra details for values from events
	events				-- toggle printing of selected events messages
	print					-- toggle printing of test.print() messages
	status					-- show status of debug message printing]],

	events = [[Test Events help: /test events [sub-command] -- open the Event Manager
Sub-commands:
	clear					-- clear all registered events
	load					-- load default events
	reg <event>			-- register an event for debugging
	save					-- save default events
	status				-- print registered events to the console
	unreg <event>			-- unregister an event for debugging
	<search string>			-- search term to filter events (pattern-matching)]],

	funcint = [[Test Function Interceptor help: /test funcint <sub-command>
Sub-commands:
	add <function>			-- add function to the list
	del <function>		-- remove function from the list
	delall				-- remove all functions from the list
	list					-- view list and status of functions to intercept
	tog <function>		-- toggle status by function or listid
	when [<function> <val>]	-- set when a function should be intercepted]],

	table = [[Test Table help: /test table [var_name] -- open the Table Viewer
	can be optionally called with a variable name to open directly to]],
}
