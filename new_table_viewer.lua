local _
test.table_viewer = {}
test.table_viewer.ui = {}
local tv = test.table_viewer
tv.minor              = 'tableviewer'
tv.failed             = false
tv.history_size       = gkini.ReadInt(test.config.major, tv.minor..'.history_size', 10)
tv.history            = unspickle(gkini.ReadString(test.config.major, tv.minor..'.history', '{}'))
tv.autoopen           = gkini.ReadInt(test.config.major, tv.minor..'.autoopen', 0) == 1
tv.recursive = true

local ui = tv.ui
ui.breadcrumbs = {}

local tconcat = table.concat
local tinsert = table.insert
local tsort   = table.sort
local xor = test.xor

local vartypes = {
	['nil']      = {1, true},
	['boolean']  = {2, true},
	['number']   = {3, true},
	['string']   = {4, true},
	['function'] = {5, false},
	['table']    = {6, true},
	['userdata'] = {7, false},
}

local function build_table_name(tree, startnode)
--test.print('---------build_table_name START ---------')
	local nameparts = {table = {}, userdata = {}}
	local curid = startnode or tonumber(tree.value)

	local function strip_label(id)
--		test.print('> strip_label START')
		local name, modname = tree['NAME'..id]
-- 		if (type(table_ref[id]) == 'table') then
-- 			return (name:gsub('%(.*%)%s', ''))
-- 		else
		modname = name:gsub('%s%(.-%)', '')
		modname = modname:gsub(':.*', '')
--		test.print('> strip_label END')
		if (ui.readiup.value == 'ON' and type(iup.TreeGetTable(tree, id)) == 'userdata') then
			if (modname == name) then return 'userdata', modname else return 'table', modname end
		else
			return 'table', modname
		end
	end

	local subtable, modname
	while (curid > 0) do
--		test.print('curid='..tostring(curid))--..' / strip_desc='..tostring(strip_label(curid))..' / type='..type(strip_label(curid)))
		--printtable{strip_label(curid)}
		
		subtable, modname = strip_label(curid)
--		test.print('subtable='..tostring(subtable)..' / str='..tostring(modname))
		tinsert(nameparts[subtable], 1, modname) -- the table is populated in reverse, so insert to position 1
--		test.print('nameparts['..subtable..'][1]='..nameparts[subtable][1])
		--if (nameparts[1] == nil) then return end
		curid = tonumber(tree['PARENT'..curid])
	end

	subtable, modname = strip_label(0)
	for i, v in ipairs(test.splittable(modname)) do
		tinsert(nameparts['table'], i, v)
	end

--	printtable(nameparts.table)
--	printtable(nameparts.userdata)
	local udata_path = tconcat(nameparts['userdata'], ' > ')
	if (udata_path ~= '') then udata_path = ' ('..udata_path..')' end
--test.print('---------build_table_name END ---------')
	return test.jointable(nameparts['table'])..udata_path
end

local function populate_tree(tree, table_name, startnode)
	if (startnode and type(startnode) ~= 'number') then error('Expected startnode to be a number') return nil end
	
	startnode = startnode or 0
--	test.print('populate_tree start: table_name='..tostring(table_name)..' / startnode='..tostring(startnode))
	local tbl_exists, tbl = test.exists(table_name)
	if (not tbl_exists) then return false end

	-- Create references for dont_read
	for k in pairs(test.dont_read) do
		local result, ref = test.exists(k)
		test.dont_read[k] = result and ref or false
	end

	local function format_val(val, vtype)
		vtype = vtype or type(val)
		if (vtype == 'string') then val = string.format('%q', val) else val = tostring(val) end
		if (vtype == 'string' or vtype == 'number') then val = ' ('..vtype..'): '..val else val = ': '..val end
		return val
	end
	
	local function iup_recurse(control, nodeid, label)
		local cur_control = control

		-- Get list of controls within the current iup container
		local controls = {}
		repeat
			tinsert(controls, cur_control)
			cur_control = iup.GetBrother(cur_control)
		until ((nodeid == -1) or (not cur_control) or label) -- End if this is the root node, we run out of brothers, or this is a named userdata.

		
		for i=#controls, 1, -1 do -- process controls table in reverse for adding to tree
			local v = controls[i]
			local value = v
			local iupType = iup.GetType(v)

			if (test.IsIupContainer(v) or (iupType == 'list' and v['control'] == 'YES')) then

-- 				if (v == breadcrumbs) then 
-- 					tree:leafadd(node, 'Breadcrumb buttons are not created until later.')
-- 					return 
-- 				end

				if (nodeid == -1) then
					tree:associate(0, value)
				else
					if (label) then value = nil end
					tree:branchadd(nodeid, label or iupType, value)
				end
				cur_control = iup.GetNextChild(v)
				if (cur_control) then iup_recurse(cur_control, nodeid + 1) end
			else
				if (nodeid == -1) then
					tree:associate(0, value)
				else
					if (label) then value = nil end
					tree:leafadd(nodeid, label or iupType, value)
				end
			end
		end
	end

	local function recurse(tbl, nodeid)
--		test.print('recurse start -- nodeid='..nodeid)
--		test.print('tbl='..tostring(tbl)..' / nodeid='..tostring(nodeid))
		
		for _, v in pairs(test.dont_read) do 
			if (tbl == v) then return end 
		end

		if (type(tbl) == 'userdata' and ui.readiup.value == 'ON' and tree['name1'] == nil) then
			iup_recurse(tbl, -1) -- Send nodeid -1 to indicate root level should be changed

		elseif (type(tbl) == 'table') then
			local keys = {}
			for k in pairs(tbl) do tinsert(keys, k) end
			tsort(keys, function(a,b) return gkmisc.strnatcasecmp(a,b) > 0 end)
--			test.print('keys='..test.stringify(keys))
--			test.print('tbl='..test.stringify(tbl))
			
			for _,v in ipairs(keys) do
				local field = tbl[v]
				local t = type(field)
				if (ui.filters[t].value == 'ON') then -- verify filter setting for variable type
--				test.print('v='..tostring(v)..' / t='..t)
				
					local label = tostring(v)
					if (t == 'table') then
						-- insert new table with special branchname key set, then recurse the branch
						
						if (type(v) == 'number') then
							label = '['..label..'] (table)'
						else
							label = label..' (table)'
						end
						tree:branchadd(nodeid, label)
						recurse(field, nodeid + 1)

					elseif (t == 'userdata' and ui.readiup.value == 'ON' and test.IsIupControl(field)) then
						label = label..' (iup.'..iup.GetType(field)..')'
						iup_recurse(field, nodeid, label) -- don't send nodeid + 1 because the decision to create a branch is first made in iup_recurse()

					else
						local val = format_val(field, t)
						-- format key name
						if (type(v) == 'number') then label = '['..label..']' end
						label = label..val
						tree:leafadd(nodeid, label)
					end
				end
			end

		else
			local val = format_val(tbl)
			return true, val
		end
		return nil -- 
	end

	local result, val = recurse(tbl, startnode)
	if (result) then 
		if (startnode > 0) then
			local name_parts = test.splittable(table_name)
			table_name = name_parts[#name_parts]
		end
		table_name = table_name..val 
	end
	tree['name'..startnode] = table_name
	return true
end
test.populate = populate_tree -- No clue why I did this.  Leaving it for now.

local function create_breadcrumbs(table_name)

	local function new_crumb(name, path)
		local button = iup.stationbutton{title=name, path=path}

		function button:action()
			ui.textbox.value = self.path
			ui.go:action()
		end
		return button
	end

	local box = ui.breadcrumbs.box
	local parent = iup.GetParent(box)

	--if (ui.breadcrumbs.list[1] == nil) then 
		ui.breadcrumbs.list = test.splittable(table_name) 
	--end
	local list = ui.breadcrumbs.list

	iup.Detach(box)
	iup.Destroy(box)
	box = iup.hbox{gap = 5,}
	
	local highindex = #list -1
	if (highindex > 0) then
		for i = 1, highindex do
			iup.Append(box, new_crumb(list[i], test.jointable(list, 1, i)))
		end
	else
		local button = new_crumb(list[1], list[1])
		button.active = 'NO'
		iup.Append(box, button)
	end
	iup.Append(box, iup.fill{})
	iup.Append(parent, box)
	ui.breadcrumbs.box = box
	parent:map()
	iup.Refresh(parent)
end


local function create_edit_box()
	local oldtype, oldval
	local vartypesel = {expand = 'YES', gap = 2} --alignment = 'ACENTER'.

	ui.edit = iup.stationbutton{title = 'Edit'} -- action declared below
	ui.multiline = iup.stationsubmultiline{size = 350, expand = 'YES', readonly = 'YES'}
	local save = iup.stationbutton{title = 'Save', active = 'NO'} -- action declared below

	-- create radio buttons based on the values of the vartypes table
	for k, v in pairs(vartypes) do
		vartypesel[ v[1] ] = iup.stationradio{title = k, active = 'NO'}
		_, vartypesel[ v[1] ].action = pcall(
			function(v1, v2, v3, v4) 
				local func = assert(loadstring([[return function(k, vartypesel, vartypes, save)
					local editstate = (test.table_viewer.ui.edit.title == 'Cancel')
					vartypesel.type = k
					if (editstate and vartypes[k][2]) then save.active = 'YES' else save.active = 'NO' end 
				end]]))()
				return function() func(v1, v2, v3, v4) end
			end, 
		k, vartypesel, vartypes, save)
	end

	ui.radio = iup.radio{iup.vbox(vartypesel), active = 'YES'}

	function ui.radio:setvalue(v)
		-- setvalue is passed the return value of type(variable) or a string describing the type() of a variable
		-- thus type(v) will always be "string"
--		test.print('self='..tostring(self)..' / v='..tostring(v))
		local t = type(v)
		if (t ~= 'string') then
			OpenAlarm('checking for non-string', 'This error was added to identify any non-string value of v, since I couldn\'t determine why I was account for possible number values.\n\nv = '..tostring(v)..'\n\n'..debug.traceback())
			return
		end

		if (t ~= 'number' and t ~= 'string') then -- if the value is nil
			self.value.value = 'OFF'
			self.value = nil
			return nil 
		end
		
		if (t == 'string') then -- get or verify the index of the variable type.
			v = vartypes[v][1]
		-- else -- why do I need this else?  v is only ever a string value
			--if (v >7 or v<1) then test.print('not in range') return nil end
		end
--		test.print('v='..tostring(v))
		self.value = vartypesel[v]
		if (v) then vartypesel[v].action() end
		return true
	end

	function ui.edit:action(controls_only)
		-- pass true in controls_only to change the controls to their default state 
		-- without resetting any changed values.  Currently used when clicking Save.
		if (self.title == 'Edit') then
			oldval = ui.multiline.value
			oldtype = vartypesel.type
			ui.multiline.readonly = 'NO'
			save.active = 'YES'
			for i, v in ipairs(vartypesel) do 
				if (vartypes[v.title][2]) then 
					v.active = 'YES' 
				else 
					if (ui.radio.value == v) then save.active = 'NO' end
				end 
			end
			self.title = 'Cancel'
		else
			if (not controls_only) then 
				ui.multiline.value = oldval
				if (vartypesel.type ~= oldtype) then ui.radio:setvalue(oldtype) end
			end
			ui.multiline.readonly = 'YES'
			save.active = 'NO'
			for i, v in ipairs(vartypesel) do v.active = 'NO' end
			self.title = 'Edit'
		end
	end

	function save:action() 
		local actions = {
			-- [1] nil
			function(v) return nil end,
			-- [2] boolean
			function(v) local bmap = {['true'] = true, ['false'] = false}; return bmap[v] end,
			-- [3] number
			function(v) return tonumber(v) end,
			-- [4] strings
			function(v) return '"'..tostring(v)..'"' end,
			-- [5] function
			function() return nil end,
			-- [6] table
			function(v) 
--				test.print('v='..v)
				local result, val = pcall(
					function(v) 
--						test.print('v2='..v)
						return unspickle(v)
					end,
					v) 
--				test.print('result='..tostring(result)..' / val='..tostring(val))
				if (result) then val = 'unspickle("'..v..'")' else val = result end
--				test.print('val='..tostring(val))
				return val
			end,
		}

		local newvalue = ui.multiline.value --new value
		local curtype = vartypesel.type
		if (curtype == 'nil' or newvalue:lower() == 'nil') then 
			newvalue = nil
			curtype = 'nil'
		else 
			newvalue = actions[ vartypes[curtype][1] ](newvalue)
			if (not newvalue) then 
				OpenAlarm('Table Viewer', 'There was an error converting the new value to the new type.', 'OK') 
				return
			end
		end

		local var = ui.selected.title
		local result, error = pcall(function() assert(loadstring(var..'='..tostring(newvalue)))() end)

		if (result) then 
			local function rebuild_node(curid, rebuild_parent)
				local node_to_rebuild = curid
				local name_to_rebuild = var
				if (rebuild_parent) then
					node_to_rebuild = tonumber(ui.tree['parent'..curid])
					if (node_to_rebuild < 0) then node_to_rebuild = 0 end
					name_to_rebuild = build_table_name(ui.tree, node_to_rebuild)
					ui.tree:clearnode(node_to_rebuild)
				end
				populate_tree(ui.tree, name_to_rebuild, node_to_rebuild)
				ui.tree.redraw = 'YES'
				ui.edit:action(true)
			end
			
			local curid = tonumber(ui.tree.value)
			if (curtype == 'boolean' or curtype == 'number' or curtype == 'string' or curtype == 'nil') then
				--if converting from a branch-type value to a leaf-type, rebuild the parent
				if (ui.tree['kind'..curid] == 'BRANCH') then rebuild_node(curid, true) else rebuild_node(curid, false) end
			else
				--if converting from a leaf-type value to a branch-type, rebuild the parent
				if (ui.tree['kind'..curid] == 'LEAF') then rebuild_node(curid, true) else rebuild_node(curid, false) end
			end
		else
			OpenAlarm('Table Viewer', error, 'OK')
		end
	end

	local edit_box = iup.hbox{
		ui.multiline,
		iup.hbox{
			iup.vbox{
				ui.edit,
				ui.radio,
				save,
				gap = 15,
			},
			iup.fill{size = 10},
		},
		gap = 5,
	}
	return edit_box
end

-- *******************************************************************************************************	
-- *******************************************************************************************************	
-- *******************************************************************************************************	

function test.table_viewer.ui:build_dialog()
	local dialog
	ui.breadcrumbs = {create = create_breadcrumbs, list = {}, box = iup.hbox{gap = 5,iup.stationbutton{title = '', active = 'NO'}}}
-- -------------------------------------
-- Navigation Bar definition        ----
	ui.entry, ui.textbox, ui.dropdown = test.list2{expand='HORIZONTAL', list = tv.history}
	ui.textbox.action = function(self, c, after) if (after == '') then ui.go.active = 'NO' else ui.go.active = 'YES' end return iup.DEFAULT end
	ui.dropdown.oldaction = ui.dropdown.action
	ui.dropdown.action = function(self, t, i, v) self:oldaction(t, i, v); if tv.autoopen then ui.go:action() end end

	ui.readiup = iup.stationtoggle{title = 'Read IUP', value = 'OFF', action = function(self) ui.go:action() end}

	ui.go = iup.stationbutton{title='Go', size=100,}
	ui.go.action = function(self, fromOpen)
		-- fromOpen should only be set if this function is called from the open control -- doesn't appear to be necessary anymore.
		local table_name = ui.textbox.value
		ui.tree:clearnode()

		if (populate_tree(ui.tree, table_name)) then
			if (ui.edit.title == 'Cancel') then ui.edit:action(true) end
			ui.tree:selection_cb(0, 1)
			ui.breadcrumbs.create(table_name)

			-- Check the history and add new search item if necessary
			local inHistory = false
			for _, v in ipairs(tv.history) do if (v == table_name) then inHistory = true; break end end
			if (not inHistory) then 
				tinsert(tv.history, 1, table_name)
				local size = tv.history_size
				if (#tv.history == (size + 1)) then tv.history[size + 1] = nil end
				do
					local h = {}
					for i = 1, size do h[i] = tv.history[i] end
					gkini.WriteString(test.config.major, tv.minor..'.history', spickle(h))
				end
			end
			for i, v in ipairs(tv.history) do ui.dropdown[i] = v end
		else
			OpenAlarm('Bad Entry', 'The table "'..table_name..'" does not exist.', 'OK')
		end

	end

	local navbar = iup.pdasubframe_nomargin{
		iup.pdasubframebg{
			iup.hbox{
				iup.label{title='Table Name:'}, ui.entry, ui.readiup, iup.fill{}, ui.go,
				gap = 5, aligntment = 'ACENTER',
			}
		}
	}


-- -------------------------------------
-- Filter-Toggle Toolbar definition ----
	ui.filters = {'boolean', 'number', 'string', 'function', 'userdata', 'thread', 'table',}
	for i, v in ipairs(ui.filters) do
		ui.filters[v] = iup.stationtoggle{title = v, value = 'ON', action = function(self) ui.go.action() end}
		-- Double reference for ease of hbox creation and filter value checking (values checked by type() return)
		ui.filters[i] = ui.filters[v]
	end
	tinsert(ui.filters, 1, iup.label{title = 'Filters:'})
	tinsert(ui.filters, iup.fill{})
	ui.filters.gap = 10
	
	local filterbar = iup.pdasubframe_nomargin{
		iup.pdasubframebg{
			iup.hbox(ui.filters)
		}
	}

-- -------------------------------------
-- Text Filter Toolbar definition   ----
-- 	local filterbar2 = iup.pdasubframe_nomargin{
-- 		iup.pdasubframebg{
-- 			iup.hbox{filtertext, filterbutton, gap = 5}
-- 		}
-- 	}

-- -------------------------------------
-- Breadcrumb Toolbar definition    ----
	local breadcrumbbar = iup.pdasubframe_nomargin{
		iup.pdasubframebg{
			iup.hbox{ui.breadcrumbs.box}
		}
	}

-- -------------------------------------
-- Location Toolbar definition      ----
	ui.open = iup.stationbutton{title = 'Open', active = 'NO'}
	ui.open.action = function(self)
		ui.breadcrumbs.list = {}
		local str = ui.selected.title
		if (test.dont_read[str]) then return end
		ui.textbox.value = str
		ui.go:action(true)
	end

	ui.selected = iup.label{title = '', expand = 'HORIZONTAL'}
	
	local locationbar = iup.pdasubframe_nomargin{
		iup.pdasubframebg{
			iup.hbox{iup.label{title = 'Currently selected: '}, ui.selected, iup.fill{}, ui.open}
		}
	}

-- -------------------------------------
-- Main Content Window definition   ----

	ui.tree = iup.stationsubtree{expand = 'YES', addexpanded = 'NO'}
	--[[
	When the tree is populated, a reference to any non-named userdata will be created in the table tree.userdata_ref
	the tree will utilize iup.TreeSetTableId() to associate non-named userdata to their entry in the table
	named data will have the name parsed and accessed via loadstring()
	]]
	ui.tree.userdata_ref = {}

	function ui.tree:associate(id, value)
		tinsert(self.userdata_ref, value)
		if (id == 0) then
--			test.print('#='..tostring(#self.userdata_ref)..' / id='..tostring(id)..' / value='..tostring(value))
--			test.print(debug.traceback())
		end
		iup.TreeSetTableId(self, id, self.userdata_ref[#self.userdata_ref])
	end

	function ui.tree:clearnode(startnode)
		startnode = startnode or 0
--		test.print('startnode='..tostring(startnode))
		local nodeid = startnode
		if (nodeid == 0) then
			for i, v in ipairs(self.userdata_ref) do
--				test.print('i='..tostring(i)..' / v='..tostring(v))
				iup.TreeSetTableId(self, iup.TreeGetTableId(self,v), nil)
			end
			self.userdata_ref = {}
			self['name'..startnode] = ''
			ui.selected.title = ''
			ui.multiline.value = ''
		else
			--If nodeid is a BRANCH, unset the TreeTableIds of the children
			if (self['kind'..nodeid] == 'BRANCH') then
				local startdepth, curdepth = tonumber(self['depth'..nodeid])
				while (true) do
					nodeid = nodeid + 1
					curdepth = tonumber(self['depth'..nodeid])
					-- stop processing the branch if the depth is the same as the starting node
					if (curdepth == startdepth) then break end
					iup.TreeSetTableId(self, curid, nil)
					-- I'm not going to bother trying to find the reference in tree.userdata_ref, since it will get reset when a clearnode(0) is run
				end
			else
				--If nodeid is a LEAF, um.... do I need to do anything with a leaf?
			end
		end
		iup.TreeSetTableId(self, startnode, nil)
		self['delnode'..startnode] = 'CHILDREN'
	end

	function ui.tree:branchadd(id, label, value)
		self['addbranch'..id] = label
		self['state'..id + 1] = 'COLLAPSED'
		if (value) then self:associate(id + 1, value) end
	end

	function ui.tree:leafadd(id, label, value)
		self['addleaf'..id] = label
		if (value) then self:associate(id + 1, value) end
	end

	function ui.tree:renamenode_cb(id, name)
		if self['KIND'..id] == 'BRANCH' then
			local state = self['STATE'..id]
			if (state == 'EXPANDED') then self:setstate(id, 'COLLAPSED') else self:setstate(id, 'EXPANDED') end
		end
	end
	
	function ui.tree:selection_cb(id, status)
		if (ui.textbox.value == '') then return iup.IGNORE end
		local name, ref, val
		if (status == 1) then
			name = build_table_name(self)

			if (id == 0) then ui.open.active = 'NO' else ui.open.active = 'YES' end
			-- no editing or jumping to the node if it's a non-named userdata
			if (name:match('%(.*%)')) then ui.open.active = 'NO'; ui.edit.active = 'NO' else ui.edit.active = 'YES' end
			
--			test.print('id = '..id..' / name = '..tostring(name))
			-- If no table_ref entry, get the ref or value form the name.  test.exists() returns the value in it's second return.
			ref = iup.TreeGetTable(self, id) or assert(function() local _, v = test.exists(name); return v end)()
			if (ref == 'attempt to call a nil value') then ref = nil end
			--test.print('ref = '..tostring(ref))

			if (ui.readiup.value == 'ON' and test.IsIupControl(ref)) then
				val = 'Name='..test.nz(iup.GetName(ref)).."\n"..string.gsub(iup.GetAttributes(ref),",", "\n")
			else
				val = tostring(ref)
			end
			--test.print('val = '..tostring(val))
			ui.multiline.value = val
			ui.radio:setvalue(type(ref))
			ui.selected.title = name
		elseif (status == 0) then
			-- Cancel any changes before switching nodes.
			if (ui.edit.title == 'Cancel') then ui.edit:action() end
		end
		return iup.DEFAULT
	end

	ui.editbox = create_edit_box()

	local content = iup.hbox{
		iup.pdasubframe_nomargin{ iup.pdasubframebg{ ui.tree } },
		iup.pdasubframe_nomargin{ iup.pdasubframebg{ ui.editbox } },
		gap = 5,
	}

-- -------------------------------------
-- Footer Toolbar definition        ----
	ui.options = iup.stationbutton{title='Options', size = 100,}
	ui.options.action = function(self)
		local opt = tv.options
		opt.build_dialog()
		opt.ui.dialog:popup(iup.CENTER, iup.CENTER)
		opt.ui.dialog:destroy()
	end


	ui.close = iup.stationbutton{title='Close', size = 100,}
	ui.close.action = function(self) HideDialog(iup.GetDialog(self)) end

	
	local footer = iup.pdasubframe_nomargin{
		iup.pdasubframebg{
			iup.hbox{iup.fill{}, ui.options, ui.close, gap = 5}
		}
	}

-- -------------------------------------
-- Dialog Assembly definition       ----
	local toolbar_order = {
		navbar,
		filterbar,
		breadcrumbbar,
		locationbar,
	}

	local main = iup.pdarootframe{
		iup.pdarootframebg{
			iup.vbox{
				iup.vbox(toolbar_order),
				content,
				footer,
				gap = 5,
			}

		},
		size="TWOTHIRDxTWOTHIRD", 
		expand="NO",
	}

	dialog = iup.dialog{
		iup.vbox{
			iup.fill{},
			iup.hbox{
				iup.fill{},
				main,
				iup.fill{},
			},
			iup.fill{},
		},
		defaultesc = ui.close,
		defaultenter = ui.go,
		bgcolor="0 0 0 128 *",
		-- fullscreen="YES",
		title = 'Table Viewer [v'..test.version..']',
		border="NO",
		resize="NO",
		menubox="YES",
		topmost="YES",
	}
	function dialog:Show(str)
		local autoset = false
		if (str ~= nil and type(str) == 'string') then ui.textbox.value = str; autoset = true end
		ShowDialog(self, iup.CENTER, iup.CENTER)
		if (autoset) then ui.go:action() end
	end
	dialog:map()
	ui.dialog = dialog
end

dofile('table_viewer_options.lua')

test.table_viewer.ui.build_dialog()