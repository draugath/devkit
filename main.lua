declare("test", {version = '0.2.7'})

dofile('vars.lua')
dofile('events.lua')
dofile('functions.lua') -- Includes User-commands
dofile('event_mgmt.lua')
dofile('new_table_viewer.lua')
dofile('func_int.lua')

print(test.fgcolor['white']..'DevKit Loaded [ v'..test.version..' ]')
