test.xor                = {}
test.nz                 = {}
test.exists             = {}
test.IsIupControl       = {}
test.IsIupContainer     = {}
test.strsplit           = {}
test.stringify          = function() end

local tinsert = table.insert
local tremove = table.remove

function test.xor(a, b) return ((a and not b) or (not a and b)) end

function test.nz(val, onnil)
	if (val == nil) then return (onnil or 'nil') else return val end
end

function test.print(...)
	if (test.debug.print) then return console_print(...) end
end

function test.varprint(sep, ...)
	local str = ''
	sep = tostring(sep)
	for n = 1, select('#', ...) do
		local e = select(n, ...)
		if (n % 2 == 0) then --if even
			str = str..test.stringify(e)..sep
		else -- if odd
			str = str..tostring(e)..'='
		end
	end
	str = str:gsub(sep..'$', '')
	return test.print(str)
end

local console_print_buffer = {}
local console_print_timer = Timer()
function test.buffered_console_print(str, delay)
	delay = delay or test.buffered_console_print_delay

	local function print_buffer()
		local buffer = console_print_buffer
		if (#buffer > 0) then console_print(buffer[1]) else return end
		if (#buffer > 0) then 
			tremove(buffer, 1)
			if (test.buffered_console_print_delay > 0) then test.print('There are '..tostring(#buffer)..' entries left.') end
			console_print_timer:SetTimeout(delay, print_buffer)
		end
	end
	tinsert(console_print_buffer, str)
	if (not console_print_timer:IsActive()) then print_buffer() end
end
	
function test.exists(val)
	if not val then return nil end
	return pcall(loadstring('return '..val))
	-- if (val) then return true, loadstring("return "..val)() end
end

function test.IsIupControl(control)
	return pcall(iup.GetType,control)
end

function test.IsIupContainer(control)
	if (type(control) ~= 'userdata') then return false end
	return test.container_types[iup.GetType(control)]
end


function test.stringify(v, nobinary, dump, count)
--Returns the string value of the parameter passed to it.
--Works with nspickle() to display the contents of tables that contain binary data
--Setting nobinary to true will strip binary data from the string form of tables.
	local fc = test.fgcolor['white']
	local function nspickle(tbl, nobinary, count)
		local msg = ''
		local indent = ''; if (dump) then indent = '\n'..string.rep('\t', count-1) end
		local idx = ''
		local n = 0
		
		for i, v in ipairs(tbl) do
			local tv = type(v)
			if (not (nobinary and (tv == 'function' or tv == 'userdata' or tv == 'thread'))) then 
				if (dump) then idx = '['..tostring(i)..']=' else idx = '' end
				msg = msg..indent..idx..test.stringify(v, nobinary, dump, count)..','
				n = i
			else
				break
			end
		end
		msg = msg:gsub(',$', ';')
		for k, v in pairs(tbl) do
			local tk = type(k)
			local tv = type(v)
			if (tk ~= 'number' or (tk == 'number' and (k < 1 or k > n))) then
				if (tk == 'number') then 
					k = '['..k..']'
				elseif (tk == 'string' and k:match('[^%w_]')) then 
					k = '["'..k..'"]'
				end
				if (not (nobinary and (tv == 'function' or tv == 'userdata' or tv == 'thread'))) then 
					msg = msg..indent..tostring(k)..'='..test.stringify(v, nobinary, dump, count)..',' 
				end
			end
		end
		msg = msg:gsub('[,;]$', '')
		return msg
	end

	local t = type(v)
	if (t == 'table') then 
		count = (count or 0) + 1
		local pre, post = '{', '}'
		if (dump) then
			--pre = string.rep('\t', count - 2)..'{'
			post = '\n'..string.rep('\t', count-2)..'}'
		end
		v = pre..nspickle(v, nobinary, count)..post
		count = count - 1
		if (count == 0) then v = v:gsub('^{(.*)}$', '%1') end
	elseif (t == 'string') then v = '"'..v..'"'
	else v = tostring(v) 
	end
	return v
end

function test.strsplit(pattern,text,num)
	num = num or -1
	local count = 1
	local result = {}
    local theStart = 1
    local theSplitStart, theSplitEnd = string.find(text,pattern,theStart)
    while theSplitStart do
		if (num > 0 and count == num) then break end
		table.insert(result,string.sub(text,theStart,theSplitStart-1))
        theStart = theSplitEnd + 1
        theSplitStart, theSplitEnd = string.find(text,pattern,theStart)
		count = count + 1
    end
    table.insert(result, string.sub(text,theStart))
    return result 
end

function test.jointable(tbl, i, j)
-- joins a table containing component parts of a table's name 
	if (type(tbl) ~= 'table') then return nil, 'Expected a table value' end
	i = i or 1
	j = j or #tbl
	local str = table.concat(tbl, '.', i, j)
	return str:gsub('%.%[', '[')
end

function test.splittable(str)
-- splits a given string representing a table name into it's component parts
	if (type(str) ~= 'string') then return nil, 'Expected a string value' end
	local parts = test.strsplit('%.', str)
	local ni = {}
	for i, v in ipairs(parts) do
		parts[i] = v:gsub('(%[.-%])', function(c)
			if (ni[i] == nil) then ni[i] = {} end
			ni.n = i
			table.insert(ni[i], c)
			return ""
		end)
	end
	do
		local inc = 0
		for i=1, (ni.n or 0) do
			if (ni[i] ~= nil) then
				for j, w in ipairs(ni[i]) do
					table.insert(parts, i+j+inc, w)
				end
			end
			inc = inc + #(ni[i] or {})
		end
	end
	return parts
end

function test.list2(params)
	-- call this function with table notation.  list2{}
	params.expand = params.expand or 'NO'
	params.list = params.list or {}
	params.size = params.size or 50
	params.value = params.value or ''
	local textbox = iup.text{size = params.size, expand = params.expand, value = params.value}
	local dropdown = iup.list{dropdown = 'YES', size = params.size + 20, expand = params.expand, fgcolor = '0 0 0 0', action = function(self, t, i, v)
		textbox.value = t
		iup.SetFocus(textbox)
	end}
	for i, v in ipairs(params.list) do dropdown[i] = v end
	local zbox = iup.zbox{
		--dropdown must be before textbox within the zbox to create the proper z-order
		iup.hbox{dropdown,},
		iup.hbox{textbox, iup.fill{size = 20}},
		all = 'YES',
		alignment = 'NW',
	}
	return zbox, textbox, dropdown
end

RegisterUserCommand("reload", function() print(test.fgcolor['white']..'Reloading...'); ReloadInterface() end)
RegisterUserCommand("test", function(_, args) 
	local fc       = test.fgcolor['white']
	local bool_map = {[true] = 'ON', [false] = 'OFF'}
	args = args or {}

	local actions  = {}
	actions.default = function() print(fc..test.help.main) end

	actions.debug  = {
		details    = function()
		                 test.debug.details = not test.debug.details
		                 test.event_mgmt.ui.details.value = bool_map[test.debug.details]
		                 test.event_mgmt.ui.details:action()
		                 print(fc.."Debug Details: "..tostring(test.debug.details))
		             end,
		events     = function()
		                 test.debug.events = not test.debug.events
		                 test.event_mgmt.ui.debugevents.value = bool_map[test.debug.events]
		                 test.event_mgmt.ui.debugevents:action()
		                 print(fc.."Debug Events: "..tostring(test.debug.events))
		             end,
		help       = function() print(fc..test.help[args[1]]) end,
		print      = function()
		                 test.debug.print = not test.debug.print
		                 print(fc.."Debug Print: "..tostring(test.debug.print))
		             end,
		status     = function() 
		                 local stat_str = "Current status:\n"..
		                     " - Debug Details = "..tostring(test.debug.details).."\n"..
		                     " - Debug Events  = "..tostring(test.debug.events).."\n"..
		                     " - Debug Print   = "..tostring(test.debug.print)
		                 print(fc..stat_str) 
		             end,
	}
	actions.debug.default = actions.debug.help

	actions.events = {
		clear      = function()
		                 test.event_mgmt.ui.clearall:action()
		                 print(fc..'All events cleared.')
		             end,
		help       = function() print(fc..test.help[args[1]]) end,
		load       = function()
		                 test.event_mgmt.ui.clearall:action()
		                 test.event_mgmt.load_defaults()
		                 print(fc..'Default events loaded')
		             end,
		reg        = function(v) test.event_mgmt.manual_toggle('reg', v) end,
		save       = function() 
		                 test.event_mgmt.ui.save:action()
		                 print(fc..'Default events saved')
		             end,
		status     = function() test.event_mgmt.event_status() end,
		unreg      = function(v) test.event_mgmt.manual_toggle('unreg', v) end,
	}
	actions.events.default = function(v)
		test.event_mgmt:apply_search(v)
		ShowDialog(test.event_mgmt.ui.dialog)
	end

	actions.funcint = {
		add        = function(v) test.func_int.add(v) end,
		del        = function(v) test.func_int.del(v) end,
		delall     = function() test.func_int.delall() end,
		help       = function() print(fc..test.help[args[1]]) end,
		list       = function() test.func_int.list() end,
		tog        = function(v) test.func_int.toggle(v) end,
		when       = function(v) test.func_int.when(v) end,
	}
	actions.funcint.default = actions.funcint.help

	actions.table  = {
		help       = function() print(fc..test.help[args[1]]) end,
	}
	actions.table.default = function(v) test.table_viewer.ui.dialog:Show(v) end

	for i, v in ipairs(args) do
		if     (i == 2 and args[1] == 'table') then break
		elseif (i == 2 and args[1] == 'events' and not actions.events[v:lower()]) then break --kludge
		elseif (i == 3 and args[2] == 'when') then 
			if (args[3] and args[4]) then 
				args[3] = {fname = args[3], when = tonumber(args[4])}
				break
			else 
				args[3] = nil
				break
			end
		elseif (i == 3) then break end
		args[i] = v:lower()
	end

	-- Execute the desired action, as defined in actions
	if (args == nil) then
		actions.default()
	elseif (actions[args[1]]) then
		if (actions[args[1]][args[2]]) then 
			actions[args[1]][args[2]](args[3])
		else
			actions[args[1]].default(args[2])
		end
	else
		actions.default()
	end

end)

